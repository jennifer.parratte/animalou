<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Disease extends Model
{
    use HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'description',
        'emergency_state',
    ];

    public function animal()
    {
        return $this->belongsToMany('App\Models\Animal');
    }

    public function symptom()
    {
        return $this->belongsToMany('App\Models\Symptom');
    }
}
