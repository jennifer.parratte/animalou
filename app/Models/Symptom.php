<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Symptom extends Model
{
    use HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'description',
    ];

    public function animal()
    {
        return $this->belongsToMany('App\Models\Animal');
    }

    public function disease()
    {
        return $this->belongsToMany('App\Models\Disease');
    }
}
